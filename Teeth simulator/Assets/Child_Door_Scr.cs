﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Child_Door_Scr : MonoBehaviour {
    
    public bool wszedl = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            wszedl = true;
         //   przekaz_strone();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            wszedl = false;
            //   przekaz_strone();
        }
    }

    public bool Get_wszedl()
    {
        return wszedl;
    }

  /*  void przekaz_strone()
    {
        transform.GetComponentInParent<drzwiscr>().Ktora_strona(strona);
    }
    */
}
