﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtScr : MonoBehaviour {

    public GameObject cel;

	
	// Update is called once per frame
	void Update () {
        transform.LookAt(new Vector3(cel.transform.position.x, transform.position.y, transform.position.z));
	}
}
