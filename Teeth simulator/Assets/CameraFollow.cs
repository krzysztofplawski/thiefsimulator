﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject cel;
    public float speed = 1f;
	
	
	// Update is called once per frame
	void Update () {

        transform.position = Vector3.Lerp(transform.position, new Vector3(cel.transform.position.x, transform.position.y, cel.transform.position.z), Time.deltaTime * speed);
	}
}
