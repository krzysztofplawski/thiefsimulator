﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drzwiscr : MonoBehaviour {

    //dla otwartych drzwi
    public GameObject drzwiobrot;
     bool wszedl = false;
     bool poruszaj = false;
    public float speed = 1f;
    Transform drzwi_trans;
     Vector3 cel = new Vector3(0, 0, 0);
     Vector3 nasz_trans_teraz;
     float strona = 90f;
     float ustawienie_startowe = 90f;
    public GameObject strona1;

    // dla zamknietych drzwi
    public bool otwarte = false;
     bool otwieraj = false;
    public float predkosc_otwierania = 1f;
    public float timer_otwierania = 1f;
    public GameObject zamknietedrzwi;

    //dla timera
    public GameObject napis;




    private void Awake()
    {
        drzwi_trans = drzwiobrot.GetComponent<Transform>();
        nasz_trans_teraz = drzwi_trans.localEulerAngles;
        if (otwarte)
        {
            zamknietedrzwi.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if (otwarte)
        {
            if (poruszaj)
                if (strona > 0f)
                {
                    if (wszedl)
                    {
                        nasz_trans_teraz = new Vector3(
                            cel.x,
                            Mathf.LerpAngle(nasz_trans_teraz.y, cel.y, speed + Time.deltaTime),
                            cel.z);
                        drzwi_trans.localEulerAngles = nasz_trans_teraz;
                        if (nasz_trans_teraz.y >= (cel.y - 1f))
                        {
                            poruszaj = false;
                        }
                    }
                    else
                    {
                        nasz_trans_teraz = new Vector3(
                            cel.x,
                            Mathf.LerpAngle(nasz_trans_teraz.y, ustawienie_startowe, speed + Time.deltaTime),
                            cel.z);
                        drzwi_trans.localEulerAngles = nasz_trans_teraz;
                        if (nasz_trans_teraz.y <= (ustawienie_startowe + 1f))
                        {
                            poruszaj = false;
                        }
                    }
                }
                else
                {
                    if (wszedl)
                    {
                        nasz_trans_teraz = new Vector3(
                            cel.x,
                            Mathf.LerpAngle(nasz_trans_teraz.y, cel.y, speed + Time.deltaTime),
                            cel.z);
                        drzwi_trans.localEulerAngles = nasz_trans_teraz;
                        if (nasz_trans_teraz.y <= (cel.y + 1f))
                        {
                            poruszaj = false;
                        }
                    }
                    else
                    {
                        nasz_trans_teraz = new Vector3(
                            cel.x,
                            Mathf.LerpAngle(nasz_trans_teraz.y, ustawienie_startowe, speed + Time.deltaTime),
                            cel.z);
                        drzwi_trans.localEulerAngles = nasz_trans_teraz;
                        if (nasz_trans_teraz.y >= (ustawienie_startowe - 1f))
                        {
                            poruszaj = false;
                        }
                    }
                }
        }
        else
        {
            if (otwieraj)
            {
                napis.GetComponent<TextMesh>().text = (timer_otwierania*10).ToString("0.0");
                timer_otwierania -= Time.deltaTime * speed;
                if (timer_otwierania <= 0f)
                {
                    otwarte = true;
                    napis.SetActive(false);
                    zamknietedrzwi.SetActive(false);
                    GetComponent<drzwiscr>().Otwieraj();
                    otwieraj = false;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Guard")
        {
            Otwieraj();
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Guard")
        {
            Zamykaj();
         }
    }

    
   public void Otwieraj()
    {
        if (otwarte)
        {
            if (strona1.GetComponent<Child_Door_Scr>().Get_wszedl()) { strona = 90f; }
            else { strona = -90f; }
            poruszaj = true;
            wszedl = true;
            cel.Set(drzwi_trans.localEulerAngles.x, drzwi_trans.localEulerAngles.y + strona, drzwi_trans.localEulerAngles.z);

        }
        else
        {
            napis.SetActive(true);
            otwieraj = true;

        }

    }

    public void Zamykaj()
    {
        Debug.Log("odszedl");
        if(otwarte)
        {

            Debug.Log("zamykadrzwi");
            poruszaj = true;
            wszedl = false;
            cel.Set(drzwi_trans.localEulerAngles.x, drzwi_trans.localEulerAngles.y - strona, drzwi_trans.localEulerAngles.z);
        }
        else
        {
            Debug.Log("Przewstal otwierac");
            napis.SetActive(false);
            timer_otwierania = 1f;
            otwieraj = false;
        }
    }

}
