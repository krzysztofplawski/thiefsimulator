﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoRzeczy : MonoBehaviour {

    
    public float czas ;
    public float waga;
    public float wartosc;

    public float timer_otwierania ;
    public GameObject napis;
    public bool pokazCzas = false;

    public struct info
    {
        float _czas;
        float _waga;
        float _wartosc;
        public info(float czas,float waga, float wartosc)
        {
            _czas = czas;
            _waga = waga;
            _wartosc = wartosc;
        }
    }   

    public info Getinfo()
    {
        return new info(czas, waga, wartosc);
    }

    public void OdpalCzas()
    {
        pokazCzas = true;
        napis.SetActive(true);
    }

    public void StopCzas()
    {
        pokazCzas = false;

        napis.SetActive(false);
        timer_otwierania = 0f;

    }
    private void Awake()
    {
        timer_otwierania = czas;
    }
    private void Update()
    {
        if (pokazCzas)
        {
            napis.GetComponent<TextMesh>().text = (timer_otwierania ).ToString("0.0");
            timer_otwierania -= Time.deltaTime ;
            if (timer_otwierania < 0f)
            {
                Destroy(transform.parent.gameObject);
            }
        }
       
    }
}
