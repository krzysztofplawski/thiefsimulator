﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]

public class PlayerMoveController : MonoBehaviour {

	// PUBLIC
	public SimpleTouchController leftController;
	//public SimpleTouchController rightController;
	public Transform headTrans;
	public float speedMovements = 5f;
    public GameObject dzwiekGrafika;

    public float glosnosc = 0f;
	//public float speedContinuousLook = 100f;
	//public float speedProgressiveLook = 3000f;

	// PRIVATE
	private Rigidbody _rigidbody;
	//[SerializeField] bool continuousRightController = true;

	void Awake()
	{
		_rigidbody = GetComponent<Rigidbody>();
	//	rightController.TouchEvent += RightController_TouchEvent;
	}

	/*public bool ContinuousRightController
	{
		set{continuousRightController = value;}
	}*/

	/*void RightController_TouchEvent (Vector2 value)
	{
		if(!continuousRightController)
		{
			UpdateAim(value);
		}
	}*/

	void Update()
	{
       // Debug.Log("y: "+leftController.GetTouchPosition.y +"    x: "+ leftController.GetTouchPosition.x);
        
        // move
        _rigidbody.MovePosition(transform.position + (transform.forward * leftController.GetTouchPosition.y * Time.deltaTime * speedMovements) +
			(transform.right * leftController.GetTouchPosition.x * Time.deltaTime * speedMovements) );
        glosnosc = Mathf.Sqrt(Mathf.Pow(leftController.GetTouchPosition.y, 2) + Mathf.Pow(leftController.GetTouchPosition.x, 2));
        if (glosnosc >= 1f)
            glosnosc = 1f;

        GetComponent<ViewAndSoundRaycast>().viewRadius = glosnosc * 2f;
       // dzwiekGrafika.transform.localScale = new Vector3(glosnosc*5f, dzwiekGrafika.transform.localScale.y, glosnosc*5f);
        // _rigidbody.MoveRotation(Quaternion.RotateTowards(transform.rotation, leftController.GetTouchPosition, Time.deltaTime * speedMovements));
        //  _rigidbody.MoveRotation(Quaternion.FromToRotation(transform.position, leftController.GetTouchPosition));

        /*	if(continuousRightController)
            {
                UpdateAim(rightController.GetTouchPosition);
            }*/
    }
    /*
	void UpdateAim(Vector2 value)
	{
		if(headTrans != null)
		{
			Quaternion rot = Quaternion.Euler(0f,
				transform.localEulerAngles.y - value.x * Time.deltaTime * -speedProgressiveLook,
				0f);

			_rigidbody.MoveRotation(rot);

			rot = Quaternion.Euler(headTrans.localEulerAngles.x - value.y * Time.deltaTime * speedProgressiveLook,
				0f,
				0f);
			headTrans.localRotation = rot;

		}
		else
		{

			Quaternion rot = Quaternion.Euler(transform.localEulerAngles.x - value.y * Time.deltaTime * speedProgressiveLook,
				transform.localEulerAngles.y + value.x * Time.deltaTime * speedProgressiveLook,
				0f);

			_rigidbody.MoveRotation(rot);
		}
	}

	void OnDestroy()
	{
		rightController.TouchEvent -= RightController_TouchEvent;
	}
    */
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Rzecz")
        {
            other.GetComponent<InfoRzeczy>().OdpalCzas();
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Rzecz")
        {
            other.GetComponent<InfoRzeczy>().StopCzas();
        }
    }
}
